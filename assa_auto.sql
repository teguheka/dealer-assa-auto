/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80015
 Source Host           : localhost:3306
 Source Schema         : assa_auto

 Target Server Type    : MySQL
 Target Server Version : 80015
 File Encoding         : 65001

 Date: 08/07/2019 04:31:02
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for c_security_role
-- ----------------------------
DROP TABLE IF EXISTS `c_security_role`;
CREATE TABLE `c_security_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of c_security_role
-- ----------------------------
BEGIN;
INSERT INTO `c_security_role` VALUES (1, '1', '2019-07-07 04:12:50', NULL, NULL, 'ROLE_ADMIN');
INSERT INTO `c_security_role` VALUES (2, '1', '2019-07-07 04:13:09', NULL, NULL, 'ROLE_MANAGER');
COMMIT;

-- ----------------------------
-- Table structure for c_security_user
-- ----------------------------
DROP TABLE IF EXISTS `c_security_user`;
CREATE TABLE `c_security_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `enabled` bit(1) NOT NULL,
  `fail_hint_count` tinyint(4) NOT NULL,
  `fail_login_count` int(11) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_at8if7a9lnl90wxllb9divpdf` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of c_security_user
-- ----------------------------
BEGIN;
INSERT INTO `c_security_user` VALUES (1, '1', '2019-07-07 04:12:20', NULL, '2019-07-07 04:14:17', 'ervinamerianti13@gmail.com', b'1', 0, 0, '2019-07-07 04:14:17', '$2a$10$nCn3BJz1UwKCSKVwSGdlCOGhrOOWzuz3D54O51uDqpyJbENpDlS3K', 'ervina');
COMMIT;

-- ----------------------------
-- Table structure for c_security_user_role
-- ----------------------------
DROP TABLE IF EXISTS `c_security_user_role`;
CREATE TABLE `c_security_user_role` (
  `user_id` bigint(20) NOT NULL,
  `role_id` bigint(20) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `FKi2mnybx2yfi7d9mdbm24dav27` (`role_id`),
  CONSTRAINT `FKdji3rbnh6x3qkel7o3oy5l9jq` FOREIGN KEY (`user_id`) REFERENCES `c_security_user` (`id`),
  CONSTRAINT `FKi2mnybx2yfi7d9mdbm24dav27` FOREIGN KEY (`role_id`) REFERENCES `c_security_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of c_security_user_role
-- ----------------------------
BEGIN;
INSERT INTO `c_security_user_role` VALUES (1, 1);
INSERT INTO `c_security_user_role` VALUES (1, 2);
COMMIT;

-- ----------------------------
-- Table structure for car
-- ----------------------------
DROP TABLE IF EXISTS `car`;
CREATE TABLE `car` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `brand` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `price_car` decimal(19,2) DEFAULT NULL,
  `price_sales` decimal(19,2) DEFAULT NULL,
  `year` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Records of car
-- ----------------------------
BEGIN;
INSERT INTO `car` VALUES (1, '1', '2019-07-07 05:00:50', NULL, NULL, 'NISSAN', 'white', 'NISSAN EVALIA 1.5 XV', 84000000.00, 115000000.00, 2012);
COMMIT;

-- ----------------------------
-- Table structure for customer
-- ----------------------------
DROP TABLE IF EXISTS `customer`;
CREATE TABLE `customer` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `contact_number` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for receipt_car
-- ----------------------------
DROP TABLE IF EXISTS `receipt_car`;
CREATE TABLE `receipt_car` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `created_date` datetime NOT NULL,
  `updated_by` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL,
  `additional_info` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `vehicle_police_number` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `sales_id` bigint(20) DEFAULT NULL,
  `chassis_number` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKp9a79u1gm55m2nh8pdq81ed09` (`sales_id`),
  CONSTRAINT `FKp9a79u1gm55m2nh8pdq81ed09` FOREIGN KEY (`sales_id`) REFERENCES `sales` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

-- ----------------------------
-- Table structure for sales
-- ----------------------------
DROP TABLE IF EXISTS `sales`;
CREATE TABLE `sales` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_by` varchar(255) COLLATE utf8mb4_general_ci NOT NULL,
  `created_date` datetime NOT NULL,
  `description` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `price` decimal(19,2) DEFAULT NULL,
  `car_id` bigint(20) DEFAULT NULL,
  `customer_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FKdthmw3da3qdl16yry208qmt0s` (`car_id`),
  KEY `FK72ep16wuoj7nllumicmk2ie3s` (`customer_id`),
  CONSTRAINT `FK72ep16wuoj7nllumicmk2ie3s` FOREIGN KEY (`customer_id`) REFERENCES `customer` (`id`),
  CONSTRAINT `FKdthmw3da3qdl16yry208qmt0s` FOREIGN KEY (`car_id`) REFERENCES `car` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

SET FOREIGN_KEY_CHECKS = 1;
