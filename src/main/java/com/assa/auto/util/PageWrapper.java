package com.assa.auto.util;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public class PageWrapper<T> extends org.springframework.data.domain.PageImpl<T> {

    public PageWrapper(final List<T> content, final Pageable pageable, final long total) {
        super(content, pageable, total);
    }

    public PageWrapper(final List<T> content) {
        super(content);
    }

    public PageWrapper(final Page<T> page, final Pageable pageable) {
        super(page.getContent(), pageable, page.getTotalElements());
    }

    public int getTotalPages() {
        return super.getTotalPages();
    }

    public long getTotalElements() {
        return super.getTotalElements();
    }

    public boolean hasNext() {
        return super.hasNext();
    }

    public boolean isLast() {
        return super.isLast();
    }

    public boolean hasContent() {
        return super.hasContent();
    }

    public List<T> getContent() {
        return super.getContent();
    }
}