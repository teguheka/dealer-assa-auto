package com.assa.auto.config;

import com.assa.auto.model.User;
import com.assa.auto.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.security.authentication.event.AuthenticationFailureBadCredentialsEvent;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationEventListener {

    private final Logger LOGGER = LoggerFactory.getLogger(AuthenticationEventListener.class);

    @Autowired private UserService userService;

    @EventListener
    public void authenticationFailed(AuthenticationFailureBadCredentialsEvent event) {
        String username = (String) event.getAuthentication().getPrincipal();
        User user = userService.findByUsername(username);
        if (user.getFailLoginCount() < 3) {
            user.setFailLoginCount(user.getFailLoginCount() + 1);
            LOGGER.info("username {} failed login attempts {}", username, user.getFailLoginCount());
        } else {
            user.setEnabled(Boolean.FALSE);
            LOGGER.info("username {} has been locked", username);
        }
        userService.update(user);
    }
}