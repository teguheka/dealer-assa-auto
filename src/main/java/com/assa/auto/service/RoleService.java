package com.assa.auto.service;

import com.assa.auto.dao.RoleDao;
import com.assa.auto.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleService {

    @Autowired private RoleDao roleDao;

    public Role finRoleById(Long id) {
        return roleDao.findOne(id);
    }

    public Role create(Role role) {
        return roleDao.save(role);
    }
}
