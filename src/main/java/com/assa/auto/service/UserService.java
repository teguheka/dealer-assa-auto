package com.assa.auto.service;

import com.assa.auto.exception.BadRequestException;
import com.assa.auto.exception.ResourceNotFoundException;
import com.assa.auto.model.Role;
import com.assa.auto.model.User;
import com.assa.auto.util.NullAwareBeanUtilsBean;
import org.apache.commons.beanutils.BeanUtilsBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import com.assa.auto.dao.UserDao;

import java.lang.reflect.InvocationTargetException;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;

@Service
public class UserService implements ApplicationListener<AuthenticationSuccessEvent> {

    @Autowired private UserDao userDao;
    @Autowired private BCryptPasswordEncoder bCryptPasswordEncoder;

    public User findByEmail(String email) {
        User user = userDao.findByEmail(email);
        if (user == null) {
            throw new ResourceNotFoundException(String.format("User with email %s not found!", email));
        }
        return user;
    }

    public User findByUsername(String username) {
        User user = userDao.findByUsername(username);
        if (user == null) {
            throw new ResourceNotFoundException(String.format("User with username %s not found!", username));
        }
        return user;
    }

    public User findOne(Long id) {
        User user = userDao.findOne(id);
        if (user == null) {
            throw new ResourceNotFoundException(String.format("User with id %d not found!", id));
        }
        return user;
    }

    public Page<User> findAll(Pageable pageable) {
        return userDao.findAll(pageable);
    }

    public Page<User> findByUsernameContainingIgnoreCase(String username, Pageable pageable) {
        return userDao.findByUsernameContainingIgnoreCase(username, pageable);
    }

    public User save(User user) {
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setEnabled(true);
        Role role = new Role("ROLE_TEACHER");
        user.setRoles(new HashSet<>(Collections.singletonList(role)));
        return userDao.save(user);
    }

    public void update(User user){
        userDao.save(user);
    }

    public void editProfile(String username, User editedUser) throws InvocationTargetException, IllegalAccessException {
        User user = findByUsername(username);
        BeanUtilsBean notNull = new NullAwareBeanUtilsBean();
        notNull.copyProperties(user, editedUser);
        userDao.save(user);
    }

    public void changePassword (String username, String newPassword, String oldPassword) {
        User user = findByUsername(username);

        if (!bCryptPasswordEncoder.matches(oldPassword, user.getPassword())) {
            throw new BadRequestException("Your old password doesn't match with existing!");
        }

        user.setPassword(bCryptPasswordEncoder.encode(newPassword));
        userDao.save(user);
    }

    @Override
    public void onApplicationEvent(AuthenticationSuccessEvent authenticationSuccessEvent) {
        String username = ((UserDetails) authenticationSuccessEvent.getAuthentication().getPrincipal()).getUsername();
        User user = userDao.findByUsername(username);
        user.setLastLogin(new Date());
        user.setFailLoginCount(0);
        userDao.save(user);
    }
}
