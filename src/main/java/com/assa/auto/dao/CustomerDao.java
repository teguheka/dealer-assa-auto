package com.assa.auto.dao;

import com.assa.auto.model.Car;
import com.assa.auto.model.Customer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CustomerDao  extends PagingAndSortingRepository<Customer, Long> {
    public Page<Customer> findByNameContainingIgnoreCase(String name, Pageable pageable);
}
