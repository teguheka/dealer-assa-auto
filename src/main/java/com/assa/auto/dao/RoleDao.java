package com.assa.auto.dao;

import com.assa.auto.model.Role;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface RoleDao extends PagingAndSortingRepository<Role, Long> {


}
