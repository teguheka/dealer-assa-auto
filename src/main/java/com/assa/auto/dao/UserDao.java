package com.assa.auto.dao;

import com.assa.auto.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface UserDao extends PagingAndSortingRepository<User, Long> {
    Page<User> findByUsernameContainingIgnoreCase(String username, Pageable pageable);
    User findByEmail(String email);

    User findByUsername(String username);

    @Override
    void delete(User user);
}
