package com.assa.auto.dao;

import com.assa.auto.model.Car;
import com.assa.auto.model.ReceiptCar;
import com.assa.auto.model.Sales;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ReceiptCarDao extends PagingAndSortingRepository<ReceiptCar, Long> {
    public Page<ReceiptCar> findBySalesId(Long id, Pageable pageable);
}
