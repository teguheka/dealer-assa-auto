package com.assa.auto.dao;

import com.assa.auto.model.Car;
import com.assa.auto.model.Customer;
import com.assa.auto.model.Sales;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SalesDao extends PagingAndSortingRepository<Sales, Long> {
    public Page<Sales> findById(Long id, Pageable pageable);
}
