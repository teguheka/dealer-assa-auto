package com.assa.auto.dao;

import java.io.Serializable;

import com.assa.auto.model.ModelBase;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BaseDao<T extends ModelBase> extends JpaRepository<T, Serializable> {
	
}
