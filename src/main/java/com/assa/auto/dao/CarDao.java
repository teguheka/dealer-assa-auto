package com.assa.auto.dao;

import com.assa.auto.model.Car;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CarDao extends PagingAndSortingRepository<Car, Long>  {
    public Page<Car> findByNameContainingIgnoreCase(String name, Pageable pageable);
}
