package com.assa.auto.model;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "customer")
public class Customer {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    private String name;

    @Column(name = "contact_number")
    private String contactNumber;

    @OneToMany(mappedBy = "customer")
    private List<Sales> purchasedCars;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }

    public List<Sales> getPurchasedCars() {
        return purchasedCars;
    }

    public void setPurchasedCars(List<Sales> purchasedCars) {
        this.purchasedCars = purchasedCars;
    }
}
