package com.assa.auto.model;

import javax.persistence.*;

@Entity
@Table(name = "receipt_car")
public class ReceiptCar extends ModelBase {

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "sales_id", referencedColumnName = "id")
    private Sales sales;

    @Column(name = "additional_info")
    private String additionalInfo;

    @Column(name = "vehicle_police_number")
    private String vehiclePoliceNumber;

    @Column(name = "chassis_number")
    private String chassisNumber;

    public Sales getSales() {
        return sales;
    }

    public void setSales(Sales sales) {
        this.sales = sales;
    }

    public String getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(String additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public String getVehiclePoliceNumber() {
        return vehiclePoliceNumber;
    }

    public void setVehiclePoliceNumber(String vehiclePoliceNumber) {
        this.vehiclePoliceNumber = vehiclePoliceNumber;
    }

    public String getChassisNumber() {
        return chassisNumber;
    }

    public void setChassisNumber(String chassisNumber) {
        this.chassisNumber = chassisNumber;
    }
}
