package com.assa.auto.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "car")
public class Car extends ModelBase {

    private String name;

    private String brand;

    private Integer year;

    private String color;

    @Column(name = "price_car")
    private BigDecimal priceCar;

    @Column(name = "price_sales")
    private BigDecimal priceSales;


    @OneToMany(mappedBy = "car")
    private List<Sales> sales;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public Integer getYear() {
        return year;
    }

    public void setYear(Integer year) {
        this.year = year;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public BigDecimal getPriceCar() {
        return priceCar;
    }

    public void setPriceCar(BigDecimal priceCar) {
        this.priceCar = priceCar;
    }

    public BigDecimal getPriceSales() {
        return priceSales;
    }

    public void setPriceSales(BigDecimal priceSales) {
        this.priceSales = priceSales;
    }

    public List<Sales> getSales() {
        return sales;
    }

    public void setSales(List<Sales> sales) {
        this.sales = sales;
    }
}
