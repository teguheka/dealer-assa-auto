package com.assa.auto.controller;

import com.assa.auto.dao.CarDao;
import com.assa.auto.dao.CustomerDao;
import com.assa.auto.dao.SalesDao;
import com.assa.auto.model.Car;
import com.assa.auto.model.Customer;
import com.assa.auto.model.Sales;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/sales")
public class SalesController {
    @Autowired private SalesDao salesDao;
    @Autowired private CarDao carDao;
    @Autowired private CustomerDao customerDao;

    private static Logger LOGGER = LoggerFactory.getLogger(SalesController.class);

    @GetMapping("/list")
    public ModelMap list(@PageableDefault(size = 10) Pageable pageable, @RequestParam(name = "value", required = false) Long value, Model model) {
        if (value != null) {
            model.addAttribute("key", value);
            return new ModelMap().addAttribute("sales", salesDao.findById(value, pageable));
        } else {
            return new ModelMap().addAttribute("sales", salesDao.findAll(pageable));
        }
    }

    @RequestMapping(value = "/form", method = RequestMethod.GET)
    public String tampilkanForm(@RequestParam(value = "id", required = false) Long id, Model m) {

        Sales sales = new Sales();
        m.addAttribute("sales", sales);

        List<Car> cars = new ArrayList<>();
        if (carDao.count() > 0) {
            Pageable pageable = new PageRequest(0, (int) carDao.count());
            Page<Car> pageCars = carDao.findAll(pageable);
            cars = pageCars.getContent();
            LOGGER.info("ini id MOBIL [{}]", cars.get(0).getId());
        }
        m.addAttribute("cars", cars);

        List<Customer> customers = new ArrayList<>();
        if (customerDao.count() > 0) {
            Pageable pageable = new PageRequest(0, (int) customerDao.count());
            Page<Customer> pageCustomers = customerDao.findAll(pageable);
            customers = pageCustomers.getContent();
            LOGGER.info("ini id Customer [{}]", customers.get(0).getId());
        }
        m.addAttribute("customers", customers);

        if (id != null) {
            sales = salesDao.findOne(id);
            if (sales != null) {
                m.addAttribute("sales", sales);
                return "sales/form";
            } else {
                return "sales/form";
            }
        }

        return "sales/form";
    }

    @RequestMapping(value = "/form", method = RequestMethod.POST)
    public String prosesForm(@ModelAttribute @Valid Sales sales, BindingResult err, SessionStatus status, Model model) {
        if (err.hasErrors()) {
            List<Car> listFakultas = new ArrayList<>();
            if (carDao.count() > 0) {
                Pageable pageable = new PageRequest(0, (int) carDao.count());
                Page<Car> pageCars = carDao.findAll(pageable);
                listFakultas = pageCars.getContent();
            }
            model.addAttribute("cars", listFakultas);
            return "/sales/form";
        }
        salesDao.save(sales);
        status.setComplete();
        return "redirect:/sales/list";
    }

    @GetMapping("/delete")
    public Object deleteConfirm(@RequestParam(value = "id") Sales sales) {
        salesDao.delete(sales);
        return "redirect:/sales/list";
    }
}
