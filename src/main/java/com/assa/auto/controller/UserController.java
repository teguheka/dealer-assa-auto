package com.assa.auto.controller;

import com.assa.auto.model.User;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.*;
import com.assa.auto.service.UserService;

import java.security.Principal;
import java.text.SimpleDateFormat;
import java.util.Date;

@Controller
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
        binder.registerCustomEditor(Date.class, new CustomDateEditor(dateFormat, true));
    }

    @GetMapping("/list")
    public String listPage(@PageableDefault(size = 10) Pageable pageable,
                           @RequestParam(required = false) String username, Model model) {

        if (StringUtils.isNotEmpty(username)) {
            model.addAttribute("username", username);
            model.addAttribute("data", userService.findByUsernameContainingIgnoreCase(username, pageable));
        } else {
            model.addAttribute("data", userService.findAll(pageable));
        }
        return "user/list";
    }

    // Change Password
    @GetMapping("/change-password")
    public String getChangePasswordPage() {
        return "user/change_password";
    }

    @PostMapping("/change-password")
    public String changePassword(Model model, @RequestParam String newPassword, @RequestParam String oldPassword,
                                 Principal principal) {
        String username = principal.getName();

        try {
            userService.changePassword(username, newPassword, oldPassword);
            model.addAttribute("success", Boolean.TRUE);
        } catch (Exception ex) {
            model.addAttribute("success", Boolean.FALSE);
            model.addAttribute("message", ex.getMessage());
        }
        return "user/change_password";
    }

    @GetMapping("/edit-profile")
    public String getEditProfilePage(Model model, Principal principal) {
        String username = principal.getName();
        User user = userService.findByUsername(username);
        model.addAttribute("user", user);
        return "user/edit_profile";
    }

    @PostMapping("/edit-profile")
    public String editProfile(@ModelAttribute(name = "user") User editedUser, BindingResult error, Model model, Principal principal) {
        if (error.hasErrors()) {
            model.addAttribute("user", editedUser);
            return "user/edit_profile";
        }
        String username = principal.getName();
        try {
            userService.editProfile(username, editedUser);
            model.addAttribute("user", editedUser);
            model.addAttribute("success", Boolean.TRUE);
        } catch (Exception ex) {
            model.addAttribute("message", ex.getMessage());
            model.addAttribute("success", Boolean.FALSE);
        }

        return "user/edit_profile";
    }
}
