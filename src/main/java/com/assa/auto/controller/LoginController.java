package com.assa.auto.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {

    @GetMapping("/login")
    public String getLoginPage(Model model, @RequestParam(defaultValue = "false", required = false) Boolean error) {
        if (error) {
            model.addAttribute("message", "Invalid user or password!");
            model.addAttribute("error", Boolean.TRUE);
        }
        return "login";
    }
}
