package com.assa.auto.controller;

import com.assa.auto.dao.CarDao;
import com.assa.auto.model.Car;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;
import org.springframework.web.servlet.ModelAndView;

import javax.validation.Valid;

@Controller
@RequestMapping("/car")
public class CarController {

    @Autowired private CarDao carDao;

    @GetMapping("/list")
    public ModelMap list(@PageableDefault(size = 10) Pageable pageable, @RequestParam(name = "value", required = false) String value, Model model) {
        if (value != null) {
            model.addAttribute("key", value);
            return new ModelMap().addAttribute("cars", carDao.findByNameContainingIgnoreCase(value, pageable));
        } else {
            return new ModelMap().addAttribute("cars", carDao.findAll(pageable));
        }
    }

    @GetMapping("/form")
    public ModelMap tampilkanForm(@RequestParam(value = "id", required = false) Car car) {
        if (car == null) {
            car = new Car();
        }
        return new ModelMap("car", car);
    }

    @PostMapping("/form")
    public String simpan(@ModelAttribute @Valid Car car, BindingResult err, SessionStatus status) {
        if (err.hasErrors()) {
            return "/car/form";
        }
        carDao.save(car);
        status.setComplete();
        return "redirect:/car/list";
    }

    @GetMapping("/delete")
    public Object deleteConfirm(@RequestParam(value = "id") Car car) {
        carDao.delete(car);
        return "redirect:/car/list";
    }

    @GetMapping("/print")
    public String print(@RequestParam(required = false) Long id, Model model) {
        if (id != null) {

        } else {
            model.addAttribute("data", carDao.findAll());
        }
        return "/car/print";
    }
}
