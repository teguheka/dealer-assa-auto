package com.assa.auto.controller;

import com.assa.auto.dao.CarDao;
import com.assa.auto.dao.CustomerDao;
import com.assa.auto.model.Car;
import com.assa.auto.model.Customer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.validation.Valid;

@Controller
@RequestMapping("/customer")
public class CustomerController {
    @Autowired
    private CustomerDao customerDao;

    @GetMapping("/list")
    public ModelMap list(@PageableDefault(size = 10) Pageable pageable, @RequestParam(name = "value", required = false) String value, Model model) {
        if (value != null) {
            model.addAttribute("key", value);
            return new ModelMap().addAttribute("customers", customerDao.findByNameContainingIgnoreCase(value, pageable));
        } else {
            return new ModelMap().addAttribute("customers", customerDao.findAll(pageable));
        }
    }

    @GetMapping("/form")
    public ModelMap tampilkanForm(@RequestParam(value = "id", required = false) Customer customer) {
        if (customer == null) {
            customer = new Customer();
        }
        return new ModelMap("customer", customer);
    }

    @PostMapping("/form")
    public String simpan(@ModelAttribute @Valid Customer customer, BindingResult err, SessionStatus status) {
        if (err.hasErrors()) {
            return "/customer/form";
        }
        customerDao.save(customer);
        status.setComplete();
        return "redirect:/customer/list";
    }

    @GetMapping("/delete")
    public Object deleteConfirm(@RequestParam(value = "id") Customer customer) {
        customerDao.delete(customer);
        return "redirect:/customer/list";
    }
}
