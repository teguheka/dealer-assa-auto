package com.assa.auto.controller;

import com.assa.auto.dao.ReceiptCarDao;
import com.assa.auto.dao.SalesDao;
import com.assa.auto.model.Customer;
import com.assa.auto.model.ReceiptCar;
import com.assa.auto.model.Sales;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.PageableDefault;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.support.SessionStatus;

import javax.validation.Valid;

@Controller
@RequestMapping("/tanda-terima")
public class ReceiptCarController {
    @Autowired private SalesDao salesDao;
    @Autowired private ReceiptCarDao receiptCarDao;

    @GetMapping("/list")
    public ModelMap list(@PageableDefault(size = 10) Pageable pageable, @RequestParam(name = "value", required = false) Long value, Model model) {
        if (value != null) {
            model.addAttribute("key", value);
            return new ModelMap().addAttribute("receiptCars", receiptCarDao.findBySalesId(value, pageable));
        } else {
            return new ModelMap().addAttribute("receiptCars", receiptCarDao.findAll(pageable));
        }
    }

    @GetMapping("/form")
    public ModelMap tampilkanForm(@RequestParam Long salesId, Model model) {
        ModelMap m = new ModelMap();
        m.addAttribute("penjualan", salesDao.findOne(salesId));
        m.addAttribute("receiptCar",  new ReceiptCar());
        return m;
    }


    @PostMapping("/form")
    public String simpan(@ModelAttribute ReceiptCar receiptCar, BindingResult err, SessionStatus status) {
        if (err.hasErrors()) {
            return "/tanda-terima/form";
        }
        Sales sales = salesDao.findOne(receiptCar.getSales().getId());
        receiptCar.setSales(sales);
        receiptCarDao.save(receiptCar);
        status.setComplete();
        return "redirect:/tanda-terima/list";
    }

}
