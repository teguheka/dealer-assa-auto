package com.assa.auto.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class BadRequestException extends RuntimeException {
    public BadRequestException() {
    }

    public BadRequestException(String string) {
        super(string);
    }

    public BadRequestException(String string, Throwable thrwbl) {
        super(string, thrwbl);
    }

    public BadRequestException(Throwable thrwbl) {
        super(thrwbl);
    }

    public BadRequestException(String string, Throwable throwable, boolean bln, boolean bln1) {
        super(string, throwable, bln, bln1);
    }
}
